#!/usr/bin/env python
# coding : utf-8

"""
author : Arnaud Sibenaler
"""

from setuptools import setup
import setuptools

setup(
    name="EX9ComplexCalculatorArnaudSibenaler",
    version="0.0.2",
    author="Arnaud Sibenaler",
    description="description test",
    license="GNU GPLv3",
    python_requires=">=3.4",
    package_dir={"": "Calculator"},
    packages=setuptools.find_namespace_packages(where="Calculator"),
)
