## Exercice 10

# Objectif
L'objectif de cet exercice est de prendre en main l'intégration continue proposée par Gitlab.

# Réalisation
On créé un fichier *.gitlab-ci.yml* dans lequel on décrit les tests à exécuter et comment générer un fichier .whl en cas de réussite.

On teste tout d'abord le programme avec *flake8* et *pytest*.
On génère ensuite les fichier .tar.gz et .whl avec les commandes :

    python3 setup.py sdist
    python3 setup.py bdist_wheel

# Notes
On a supprimé les versions dans le fichier requirements.txt pour que pip installe la version des packages la plus adaptée.
