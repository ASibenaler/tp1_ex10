#!/usr/bin/env python
# coding : utf-8

"""
author : Arnaud Sibenaler
"""

import unittest
import logging

from Calculator.Calculator.Calculator import SimpleComplexCalculator


class Test_sum(unittest.TestCase):
    """Test methode fsum"""

    def setUp(self):
        self.calculator = SimpleComplexCalculator()
        logging.basicConfig(filename="tests.log", level=logging.INFO)

    def test_sum_int(self):
        res = self.calculator.fsum([1, 2], [3, 4])
        self.assertEqual(res, [4, 6])
        logging.info("Test somme int OK")

    '''def test_sum_float(self):
        res = self.calculator.fsum([1.1, 2.2], [3.3, 4.4])
        self.assertEqual(res, [4.4, 6.6])
        logging.info("Test somme float OK")'''

    def test_sum_str(self):
        res = self.calculator.fsum([1, 2], [3, "a"])
        self.assertEqual(res, "ERROR")
        logging.info("Test somme int-char ERREUR")


class Test_substract(unittest.TestCase):
    """Test methode substract"""

    def setUp(self):
        self.calculator = SimpleComplexCalculator()
        logging.basicConfig(filename="tests.log", level=logging.INFO)

    def test_sub_int(self):
        res = self.calculator.substract([1, 2], [3, 4])
        self.assertEqual(res, [-2, -2])
        logging.info("Test soustraction int OK")

    '''def test_sub_float(self):
        res = self.calculator.substract([1.1, 2.2], [3.3, 4.4])
        self.assertEqual(res, [-2.2, -2.2])
        logging.info("Test soustraction float OK")'''

    def test_sub_str(self):
        res = self.calculator.substract([1, 2], [3, "a"])
        self.assertEqual(res, "ERROR")
        logging.info("Test soustraction int-char ERREUR")


class Test_multiply(unittest.TestCase):
    """Test methode multiply"""

    def setUp(self):
        self.calculator = SimpleComplexCalculator()
        logging.basicConfig(filename="tests.log", level=logging.INFO)

    def test_mul_int(self):
        res = self.calculator.multiply([1, 2], [3, 4])
        self.assertEqual(res, [-5, 10])
        logging.info("Test produit int OK")

    '''def test_mul_float(self):
        res = self.calculator.multiply([1.1, 2.2], [3.3, 4.4])
        self.assertEqual(res, [-6.05, 12.1])
        logging.info("Test produit float OK")'''

    def test_mul_str(self):
        res = self.calculator.multiply([1, 2], [3, "a"])
        self.assertEqual(res, "ERROR")
        logging.info("Test produit int-char ERREUR")


class Test_divide(unittest.TestCase):
    """Test methode divide"""

    def setUp(self):
        self.calculator = SimpleComplexCalculator()
        logging.basicConfig(filename="tests.log", level=logging.INFO)

    def test_div_int(self):
        res = self.calculator.divide([1, 2], [3, 4])
        self.assertEqual(res, [0.44, 0.4])
        logging.info("Test division int OK")

    '''def test_div_float(self):
        res = self.calculator.divide([1.1, 2.2], [3.3, 4.4])
        self.assertEqual(res, [0.44, 0.4])
        logging.info("Test division float OK")'''

    def test_div_str(self):
        res = self.calculator.divide([1, 2], [3, "a"])
        self.assertEqual(res, "ERROR")
        logging.info("Test division int-char ERREUR")

    def test_div_zero(self):
        res = self.calculator.divide([1, 2], [0, 0])
        self.assertEqual(res, "Division par 0 !")
        logging.info("Test division par 0 ERREUR")


if __name__ == "__main__":
    unittest.main()
